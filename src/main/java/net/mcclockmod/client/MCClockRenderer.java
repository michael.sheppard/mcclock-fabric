/*
 * MCClockRenderer.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.mcclockmod.client;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.mcclockmod.mixin.MCMinecraftClientAccessor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unused")

//  Colors
//  (Minecraft Name)	Chat Code	MOTD Code	Decimal	Hexadecimal
//  (dark_red)			§4	        \u00A74	    11141120	AA0000
//  (red)				§c	        \u00A7c	    16733525	FF5555
//  (gold)				§6	        \u00A76	    16755200	FFAA00
//  (yellow)			§e	        \u00A7e	    16777045	FFFF55
//  (dark_green)		§2	        \u00A72	    43520		00AA00
//  (green)				§a	        \u00A7a	    5635925		55FF55
//  (aqua)				§b	        \u00A7b	    5636095		55FFFF
//  (dark_aqua)			§3	        \u00A73	    43690		00AAAA
//  (dark_blue)			§1	        \u00A71	    170			0000AA
//  (blue)				§9	        \u00A79	    5592575		5555FF
//  (light_purple)		§d	        \u00A7d	    16733695	FF55FF
//  (dark_purple)		§5	        \u00A75	    11141290	AA00AA
//  (white)				§f	        \u00A7f	    16777215	FFFFFF
//  (gray)				§7	        \u00A77	    11184810	AAAAAA
//  (dark_gray)			§8	        \u00A78	    5592405		555555
//  (black)				§0	        \u00A70	    0			000000
@Environment(EnvType.CLIENT)
public class MCClockRenderer extends InGameHud {
    private static final int COLOR_RED = 0xffff5555; // ARGB
    private static final int COLOR_GREEN = 0xff55ff55;
    private static final int COLOR_WHITE = 0xffffffff;
    private static final int MAX_TICKS = 24000;
    private static final double TICKS_TO_SECONDS = 3.6;

    private static boolean isVisible = true;
    private static boolean realTime = false;
    private static boolean hudVisible = true;

    private static int count = 0;
    private double x, y, z;
    private double homeDistance;
    private double homeDirection;
    private float temperature;
    private double compassHeading;
    private String biomeName;

    // keypress debounce, issue started in fabric 207
    private static long lastDebounceTime = 0;
    private static final long DEBOUNCE_DELAY = 250; // milliseconds

    private static final MinecraftClient CLIENT = MinecraftClient.getInstance();

    public MCClockRenderer(MinecraftClient client) {
        super(client);
    }

    public void renderHUD(MatrixStack matrixStack, float deltaTicks) {
        TextRenderer fontRenderer = CLIENT.textRenderer;
        if (CLIENT.options.debugEnabled || !isVisible) {
            return;
        }

        PlayerEntity player = CLIENT.player;
        ClientWorld world = CLIENT.world;

        if (world != null && player != null) {
            int y0 = CLIENT.getWindow().getScaledHeight() - 11;
            int textY = y0 - (fontRenderer.fontHeight / 2);

            int width = CLIENT.getWindow().getScaledWidth();

            matrixStack.push();

            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();

            if (count % 10 == 0) {
                Vec3d v = player.getPos();
                x = v.x;
                y = v.y;
                z = v.z;
                homeDistance = getHomeDistance();
                homeDirection = getHomeDirection(player.getYaw());

                BlockPos bp = new BlockPos(x, y, z);
                Biome theBiome = CLIENT.world.getBiome(bp);
                if (theBiome != null) {
                    Identifier id = CLIENT.world.getRegistryManager().get(Registry.BIOME_KEY).getId(theBiome);
                    if (id != null) {
                        biomeName = id.getPath();//theBiome.getCategory().getName();
                    }
                    temperature = theBiome.getTemperature(bp);
                }
                compassHeading = calcCompassHeading(player.getYaw());
            }
            count++;

            // draw the text
            String timeStr;
            if (realTime) {
                timeStr = formatRealTime();
            } else {
                timeStr = formatMinecraftTime(CLIENT.world.getTimeOfDay());
            }
            String timeFormatted = String.format("%s§c%s", (realTime ? "Real Time: " : "Minecraft Time: "), timeStr);
            int x0 = width - fontRenderer.getWidth("XXXXX.X/XXXXX.X/XXXXX.X________");
            fontRenderer.draw(matrixStack, timeFormatted, x0, textY, COLOR_WHITE);

            if (hudVisible) {
                String playerPos = String.format("Position: §c% 5.1f/§a% 5.1f§c/§b% 5.1f", x, y, z);
                String tc = (homeDirection < 2.5 && homeDirection > -2.5) ? "§a" : (homeDirection < 15.0 && homeDirection > -15.0) ? "§e" : (homeDirection < 177.5 && homeDirection > -177.5) ? "§c" : "§9";
                String arrows = String.format((homeDirection < 0.0) ? "<<%5.1f" : (homeDirection == 0.0 || homeDirection == 180.0) ? "%5.1f" : ">>%5.1f", Math.abs(homeDirection));
                String homeDist = String.format("Home Dist/Dir: §c%5.1f / %s%s", homeDistance, tc, arrows);
                String heading = String.format("Heading: §3% 5.1f", compassHeading);
                String biome = String.format("Biome: §3%s", biomeName);
                String biome_temp = String.format("Temperature: §3% 5.2f", temperature);
                String fps = String.format("FPS: §a% 3d §c(% 3d)", MCClockModClient.getClientTickHandler().getAverageFps(), ((MCMinecraftClientAccessor)CLIENT).getCurrentFPS());

                fontRenderer.draw(matrixStack, playerPos, x0, (textY - fontRenderer.fontHeight), COLOR_WHITE);
                fontRenderer.draw(matrixStack, homeDist, x0, (textY - (fontRenderer.fontHeight * 2)), COLOR_WHITE);
                fontRenderer.draw(matrixStack, heading, x0, (textY - (fontRenderer.fontHeight * 3)), COLOR_WHITE);
                fontRenderer.draw(matrixStack, biome, x0, (textY - (fontRenderer.fontHeight * 4)), COLOR_WHITE);
                fontRenderer.draw(matrixStack, biome_temp, x0, (textY - (fontRenderer.fontHeight * 5)), COLOR_WHITE);
                fontRenderer.draw(matrixStack, fps, x0, (textY - (fontRenderer.fontHeight * 6)), COLOR_WHITE);
            }
            RenderSystem.disableBlend();
            matrixStack.pop();
        }
    }

    public String formatMinecraftTime(double ticks) {
        double realSeconds = (ticks % MAX_TICKS) * TICKS_TO_SECONDS; // ticks to seconds wrapping ticks if necessary
        int McHours = MathHelper.floor(((realSeconds / 3600.0) + 6) % 24); // Minecraft ticks are offset from calendar clock by 6 hours
        int McMinutes = MathHelper.floor((realSeconds / 60.0) % 60);
        int McSeconds = MathHelper.floor(realSeconds % 60);
        return String.format("%02d:%02d:%02d", McHours, McMinutes, McSeconds);
    }

    public String formatRealTime() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return String.format("%s", sdf.format(date));
    }

    // Thanks to Pythagoras we can calculate the distance to home/spawn
    private double getHomeDistance() {
        BlockPos blockpos = CLIENT.world != null ? CLIENT.world.getSpawnPos() : new BlockPos(0, 0, 0);
        Vec3d v = CLIENT.player != null ? CLIENT.player.getPos() : new Vec3d(0, 0, 0);
        double a = Math.pow(blockpos.getZ() - v.z, 2);
        double b = Math.pow(blockpos.getX() - v.x, 2);
        return Math.sqrt(a + b);
    }

    // difference angle in degrees the player is facing from the home point.
    // zero degrees means the player is facing the home point.
    // the home point is usually the world spawn point
    private double getHomeDirection(double yaw) {
        BlockPos blockpos = CLIENT.world != null ? CLIENT.world.getSpawnPos() : new BlockPos(0, 0, 0);
        Vec3d v = CLIENT.player != null ? CLIENT.player.getPos() : new Vec3d(0, 0, 0);
        double delta = Math.atan2(blockpos.getZ() - v.z, blockpos.getX() - v.x);
        double relAngle = delta - Math.toRadians(yaw);
        return MathHelper.wrapDegrees(Math.toDegrees(relAngle) - 90.0); // degrees
    }

    private double calcCompassHeading(double yaw) {
        return (((yaw + 180.0) % 360) + 360) % 360;
    }

    public static void toggleClock() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            isVisible = !isVisible;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

    public static void toggleRealTime() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            realTime = !realTime;
            lastDebounceTime = System.currentTimeMillis();
        }
    }

    public static void toggleHUD() {
        long millis = System.currentTimeMillis();
        if ((millis - lastDebounceTime) > DEBOUNCE_DELAY) {
            hudVisible = !hudVisible;
            lastDebounceTime = System.currentTimeMillis();
        }
    }
}
