/*
 * MCClockModClient.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.mcclockmod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import org.lwjgl.glfw.GLFW;


@Environment(EnvType.CLIENT)
public class MCClockModClient implements ClientModInitializer {
    private static final String CATEGORY = "key.category.mcclockmod.general";
    private static KeyBinding TOGGLE_CLOCK_VISIBILITY;
    private static KeyBinding TOGGLE_CLOCK_LOCALTIME;
    private static KeyBinding TOGGLE_CLOCK_HUD;
    private static final MCClockRenderer hud = new MCClockRenderer(MinecraftClient.getInstance());
    private static final MCClientTickHandler clientTickHandler = new MCClientTickHandler();

    @Override
    public void onInitializeClient() {
        System.out.println("Client side Mod Initializer");
        TOGGLE_CLOCK_VISIBILITY = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.mcclockmod.toggle_clock", GLFW.GLFW_KEY_V, CATEGORY));
        TOGGLE_CLOCK_LOCALTIME = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.mcclockmod.toggle_real_time", GLFW.GLFW_KEY_R, CATEGORY));
        TOGGLE_CLOCK_HUD = KeyBindingHelper.registerKeyBinding(new KeyBinding("key.mcclockmod.toggle_hud", GLFW.GLFW_KEY_PERIOD, CATEGORY));

        HudRenderCallback.EVENT.register(hud::renderHUD);

        getClientTickHandler().onClientInitialize();

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_CLOCK_VISIBILITY.isPressed()) {
                MCClockRenderer.toggleClock();
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_CLOCK_HUD.isPressed()) {
                MCClockRenderer.toggleHUD();
            }
        });

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (TOGGLE_CLOCK_LOCALTIME.isPressed()) {
                MCClockRenderer.toggleRealTime();
            }
        });
    }

    public static MCClientTickHandler getClientTickHandler() {
        return clientTickHandler;
    }
}
