/*
 * MCClockMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.mcclockmod.common;

import net.fabricmc.api.ModInitializer;

@SuppressWarnings("unused")
public class MCClockMod implements ModInitializer {
    static final String MODID = "mcclockmod";

    @Override
    public void onInitialize() {
        System.out.println("Main Mod Initializer");
        // nothing to do here
    }
}
